﻿using SimpleNavigation.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Controls;
using System.Windows.Media;

namespace SimpleNavigation.Service
{
    /// <summary>
    /// For Simplicity of the exaple, the service will be a singleton
    /// </summary>
    public class NavigationService
    {

        public List<NavigationModel> NavigationOptions { get=>NavigationNameToUserControl.Keys.ToList(); }

        public UserControl NavigateToModel(NavigationModel _navigationModel)
        {
            if (_navigationModel is null) 
                //Or throw exception
                return null;
            if (NavigationNameToUserControl.ContainsKey(_navigationModel))
            {
                return NavigationNameToUserControl[_navigationModel].Invoke();
            }
            //Ideally you should throw here Custom Exception
            return null;
        }

        //Usage of the Func, provides each call new initialization of the view
        //If you need initialized views, just remove the Func
        //-------------------------------------------------------------------
        //Readonly is used only for performance reasons
        //Of course there is option to add the elements to the collection, if dynamic navigation mutation is needed
        private readonly Dictionary<NavigationModel, Func<UserControl>> NavigationNameToUserControl = new Dictionary<NavigationModel, Func<UserControl>>
        {
            { new NavigationModel("Gène","This will navigate to the gene View",Brushes.Aqua), ()=>{ return new View.ViewA(); } },
            { new NavigationModel("Protéine","This will navigate to the protein View",Brushes.GreenYellow), ()=>{ return new View.ViewB(); } },
            { new NavigationModel("Séquence","This will navigate to the prot sequence View",Brushes.GreenYellow), ()=>{ return new View.ViewC(); } },
            { new NavigationModel("Structure","This will navigate to the prot structure View",Brushes.GreenYellow), ()=>{ return new View.ViewD(); } },
            { new NavigationModel("réaction","This will navigate to the reaction View",Brushes.GreenYellow), ()=>{ return new View.ViewE(); } },
            { new NavigationModel("voie","This will navigate to the pathway View",Brushes.GreenYellow), ()=>{ return new View.ViewF(); } },
            { new NavigationModel("utilisateur","This will navigate to the user form View",Brushes.GreenYellow), ()=>{ return new View.ViewG(); } },
            { new NavigationModel("Recherche","This will navigate to the search form View",Brushes.GreenYellow), ()=>{ return new View.ViewSearch(); } },
            { new NavigationModel("Ajouter","This will navigate to the add form View",Brushes.GreenYellow), ()=>{ return new View.ViewAdd(); } }
        };

        #region SingletonThreadSafe
        private static readonly object Instancelock = new object();
        
        private static NavigationService instance = null;

        public static NavigationService GetInstance
        {
            get
            {
                if (instance == null)
                {
                    lock (Instancelock)
                    {
                        if (instance == null)
                        {
                            instance = new NavigationService();
                        }
                    }
                }
                return instance;
            }
        }
        #endregion
    }
}
