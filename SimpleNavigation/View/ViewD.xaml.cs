﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MindFusion.Diagramming.Wpf;
using System.Collections;
using MoleculeViewer;
using System.IO;
using MindFusion.Vsx;
using System.Globalization;
using Rect = System.Windows.Rect;
using Point = System.Windows.Point;
using Ellipse = System.Windows.Shapes.Ellipse;
using static System.Net.WebRequestMethods;
using System.Xml;
using System.Data.SqlClient;
using System.Data;

namespace SimpleNavigation.View
{
    /// <summary>
    /// Logique d'interaction pour ViewD.xaml
    /// </summary>
    public partial class ViewD : UserControl
    {
        private Diagram diagram;
        private Brush linkBrush;
        private Brush tickLinkBrush;

        ArrayList linkers;
        Queue<Linker> queue;
        Queue<Linker> linkedNodes;

        public ViewD()
        {
            InitializeComponent();

            category_select.Items.Clear();
            SqlConnection con = new SqlConnection("Data Source=DESKTOP-DDTNQVJ\\SQLEXPRESS;Initial Catalog=fgtm;Integrated Security=True");
            try
            {
                con.Open();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
            try
            {
                SqlDataAdapter category_data = new SqlDataAdapter("SELECT * FROM Structure", con);
                DataSet ds = new DataSet();
                category_data.Fill(ds, "t");

                category_select.DataContext = ds.Tables["t"].DefaultView;
                category_select.DisplayMemberPath = "id";
                category_select.SelectedValuePath = "id";

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }

            diagram = new Diagram();
            view3D.Diagram = diagram;
            view3D.Scale = 1000;
            view3D.RotationAngle = 0;
            view3D.NodeThickness = 0;
            view3D.LinkScalingMode = ScalingMode.None;

            linkBrush = new SolidColorBrush(Colors.DarkSlateBlue);
            tickLinkBrush = new SolidColorBrush(Colors.ForestGreen);


            linkers = new ArrayList();
            queue = new Queue<Linker>();
            linkedNodes = new Queue<Linker>();


            view3D.Lights.Clear();
            view3D.Lights.Add(new AmbientLight());
        }

        private void SetRotationCenter(Point3D[] positions)
        {
            DoubleCollection xCol = new DoubleCollection();
            DoubleCollection yCol = new DoubleCollection();
            DoubleCollection zCol = new DoubleCollection();
            for (int i = 0; i < positions.Length; i++)
            {
                xCol.Add(positions[i].X);
                yCol.Add(positions[i].Y);
                zCol.Add(positions[i].Z);
            }
        }

        private void LoadEthane()
        {
            Dictionary<Point3D, string> positions = Readpdb(GetpdbName());

            ShapeNode zNode = null;
            ShapeNode cNode = null;

            Brush brush1 = NodeBrush(new Size(15, 15), "Red");

            foreach (var item in positions)
            {
                cNode = CreateShapeNode(item.Key, new Rect(0, 0, 20, 20), brush1, item.Value);
                zNode = cNode;
            }

        }

        private ShapeNode CreateShapeNode(Point3D position, Rect bounds, Brush brush, string text)
        {
            return CreateShapeNode(position, bounds, brush, text, false);
        }

        private ShapeNode CreateShapeNode(Point3D position, Rect bounds, Brush brush, string text, bool stroked)
        {
            double coef = 3;
            Point3D p = new Point3D(position.X * coef, position.Y * coef, position.Z * coef);
            ShapeNode node = new ShapeNode()
            {
                Shape = Shapes.Ellipse,
                Brush = brush,
                StrokeThickness = stroked ? 1 : 0,
                Bounds = bounds,
                Text = text,
                TextAlignment = TextAlignment.Center,
                TextVerticalAlignment = AlignmentY.Center,
            };
            node.Text = "";
            DiagramView3D.SetPosition3D(node, p);
            diagram.Items.Add(node);
            return node;
        }

        private VisualBrush NodeBrush(Size size, string color)
        {
            Color lightColor = new Color();
            Color borderColor = new Color();

            borderColor = Colors.Black;

            switch (color)
            {
                case "Gray":
                    lightColor = Colors.Gray;
                    break;
                case "Black":
                    lightColor = Colors.Black;
                    break;

                case "Blue":
                    lightColor = Colors.Blue;
                    break;

                case "Red":
                    lightColor = Colors.Red;
                    break;

                case "Orange":
                    lightColor = Colors.Orange;
                    break;

                case "Brown":
                    lightColor = Colors.Brown;
                    break;

                case "Yellow":
                    lightColor = Colors.Yellow;
                    break;

                default:
                    MessageBox.Show("not a valid color");
                    break;
            }



            Grid g = new Grid();
            RadialGradientBrush radialBrush = new RadialGradientBrush()
            {
                GradientOrigin = new Point(0.5, 1),
                Center = new Point(0.5, 1),
                RadiusX = 1.2,
                RadiusY = 1.2,
            };

            GradientStop stop1 = new GradientStop() { Offset = 0.2, Color = lightColor };
            radialBrush.GradientStops.Add(stop1);


            Ellipse ellipse = new Ellipse()
            {
                Fill = radialBrush,
                Width = size.Width,
                Height = size.Height,
            };
            g.Children.Add(ellipse);



            RadialGradientBrush shadowBrush = new RadialGradientBrush();
            GradientStop shadow1 = new GradientStop() { Offset = 0.85, Color = Color.FromArgb(0, 0, 0, 0) };
            GradientStop shadow2 = new GradientStop() { Offset = 1, Color = Color.FromArgb(255, 0, 0, 0) };
            shadowBrush.GradientStops.Add(shadow1);
            shadowBrush.GradientStops.Add(shadow2);

            Ellipse shadowEllipse = new Ellipse()
            {
                StrokeThickness = 2,
                Fill = shadowBrush,
                Stroke = new SolidColorBrush(borderColor),
                Width = size.Width,
                Height = size.Height,
            };
            g.Children.Add(shadowEllipse);



            LinearGradientBrush glowBrush = new LinearGradientBrush();
            glowBrush.StartPoint = new Point(0, 0);
            glowBrush.EndPoint = new Point(0, 1);
            GradientStop glow1 = new GradientStop() { Offset = 0, Color = Color.FromArgb(255, 255, 255, 255) };
            GradientStop glow2 = new GradientStop() { Offset = 0.7, Color = Color.FromArgb(0, 255, 255, 255) };
            glowBrush.GradientStops.Add(glow1);
            glowBrush.GradientStops.Add(glow2);

            Ellipse glowEllipse = new Ellipse()
            {
                Margin = new Thickness(0, 2, 0, 0),
                Fill = glowBrush,
                Stroke = null,
                Width = size.Width * 0.7,
                Height = size.Height * 0.6,
                VerticalAlignment = System.Windows.VerticalAlignment.Top,
            };

            g.Children.Add(glowEllipse);

            VisualBrush brush = new VisualBrush();
            brush.Visual = g;
            return brush;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            for (int i = diagram.Items.Count - 1; i >= 0; i--)
                diagram.Items.Remove(diagram.Items[i]);

            Button button = sender as Button;
            if (button.Name == "Ethane")
            {
                view3D.Scale = 800;
                LoadEthane();
            }
        }

        private Dictionary<Point3D, string> Readpdb(string file)
        {
            Dictionary<Point3D, string> atodico = new Dictionary<Point3D, string>();
            string workingDirectory = Environment.CurrentDirectory;
            string path = Directory.GetParent(workingDirectory).Parent.Parent.FullName + "\\pdb\\" + file;
            int counter = 0;

            foreach (string line in System.IO.File.ReadLines(path))
            {
                if (line.StartsWith("ATOM"))
                {
                    string[] sline = line.Split(new char[0], StringSplitOptions.RemoveEmptyEntries);
                    if (sline[2].StartsWith("C"))
                    {
                        atodico.Add(new Point3D(double.Parse(sline[6], CultureInfo.InvariantCulture),
                        double.Parse(sline[7], CultureInfo.InvariantCulture),
                        double.Parse(sline[8], CultureInfo.InvariantCulture)), sline[2]);
                        counter++;
                    }    
                }
            }

            return atodico;
        }

        private string GetpdbName()
        {
            BDDConnexion conn = new BDDConnexion();
            conn.connexionDirecte();
            conn.ConnectionConfig();

            try
            {
                using (SqlConnection connection = new SqlConnection(conn.connexion2))
                {
                    connection.Open();
                    string sql2 = "Select * from Structure where id = " + category_select.Text;
                    using (SqlCommand command = new SqlCommand(sql2, connection))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                string param3 = reader.GetString(2);
                                return param3;
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.ToString());
            }

            return null;
        }
    }
}
