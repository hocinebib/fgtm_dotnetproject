﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SimpleNavigation.View
{
    /// <summary>
    /// Logique d'interaction pour ViewSearch.xaml
    /// </summary>
    public partial class ViewSearch : UserControl
    {
        public ViewSearch()
        {
            InitializeComponent();
        }

        private void TextBox_GotKeyboardFocus(object sender, KeyboardFocusChangedEventArgs e)
        {
            TextBox txtBox = sender as TextBox;
            if (txtBox.Text == "Recherche...")
                txtBox.Text = string.Empty;
        }

        private void OnClick1(object sender, RoutedEventArgs e)
        {
            BDDConnexion conn = new BDDConnexion();
            conn.connexionDirecte();
            conn.ConnectionConfig();

            try
            {
                using (SqlConnection connection = new SqlConnection(conn.connexion2))
                {
                    connection.Open();
                    string sql2 = $"Select * from {((ComboBoxItem)category_select.SelectedItem).Content.ToString()} WHERE nom LIKE '%{searchval.Text}%'";
                    using (SqlCommand command = new SqlCommand(sql2, connection))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                string param1 = reader["id"].ToString(); //type int de base
                                string param2 = reader.GetString(1); //type string de base
                                string param4 = reader["statut"].ToString();
                                param4 = (param4 == "0") ? "Not Reviewed" : "Reviewed";
                                Add_line(param1, param2, param4, ((ComboBoxItem)category_select.SelectedItem).Content.ToString());
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void Add_line(string id, string gene, string rev, string typee)
        {
            var rowGroup = geneTable.RowGroups.FirstOrDefault();

            if (rowGroup != null)
            {
                TableRow row = new TableRow();

                TableCell cell = new TableCell();

                row.Background = System.Windows.Media.Brushes.LightSkyBlue;

                cell.Blocks.Add(new Paragraph(new Run(id)));
                row.Cells.Add(cell);

                cell = new TableCell();
                cell.Blocks.Add(new Paragraph(new Run(gene)));
                row.Cells.Add(cell);

                cell = new TableCell();
                cell.Blocks.Add(new Paragraph(new Run(rev)));
                row.Cells.Add(cell);

                cell = new TableCell();
                cell.Blocks.Add(new Paragraph(new Run(typee)));
                row.Cells.Add(cell);

                rowGroup.Rows.Add(row);
            }
        }

    }
}
