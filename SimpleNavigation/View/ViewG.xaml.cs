﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Xml.Linq;

namespace SimpleNavigation.View
{
    /// <summary>
    /// Logique d'interaction pour ViewG.xaml
    /// </summary>
    public partial class ViewG : UserControl
    {
        public ViewG()
        {
            InitializeComponent();
        }

        private int getID()
        {
            BDDConnexion conn = new BDDConnexion();
            conn.connexionDirecte();
            conn.ConnectionConfig();

            try
            {
                using (SqlConnection connection = new SqlConnection(conn.connexion2))
                {
                    connection.Open();
                    string sql2 = "SELECT TOP 1 * FROM table_utilisateur ORDER BY id DESC";
                    using (SqlCommand command = new SqlCommand(sql2, connection))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                int param1 = Int32.Parse(reader["id"].ToString()); //type int de base
                                return param1;
                            }
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                MessageBox.Show(e.ToString());
            }
            return -1;
        }

        private void clickreg(object sender, RoutedEventArgs e)
        {
            BDDConnexion conn = new BDDConnexion();
            conn.connexionDirecte();
            conn.ConnectionConfig();

            try
            {
                using (SqlConnection connection = new SqlConnection(conn.connexion2))
                {
                    using (SqlCommand command = new SqlCommand())
                    {
                        command.Connection = connection;            // <== lacking
                        command.CommandType = CommandType.Text;
                        command.CommandText = "INSERT into table_utilisateur (id, pseudo, email, reviewer) VALUES (@id, @pseudo, @email, @rev)";
                        command.Parameters.AddWithValue("@id", getID() + 1);
                        command.Parameters.AddWithValue("@pseudo", txtPseudo.Text);
                        command.Parameters.AddWithValue("@email", txtMail.Text);
                        if ((bool)(checkBoxx.IsChecked))
                        {
                            command.Parameters.AddWithValue("@rev", 0);
                            MessageBox.Show($"Ajout des données {getID() + 1} - {txtPseudo.Text} - {txtMail.Text} - 0");
                        }
                        else
                        {
                            command.Parameters.AddWithValue("@rev", 1);
                            MessageBox.Show($"Ajout des données {getID() + 1} - {txtPseudo.Text} - {txtMail.Text} - 1");
                        }
                        try
                        {
                            connection.Open();
                            int recordsAffected = command.ExecuteNonQuery();
                        }
                        catch (SqlException exep)
                        {
                            MessageBox.Show(exep.ToString());
                        }
                        finally
                        {
                            connection.Close();
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }
    }
}
