﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SimpleNavigation.View
{
    /// <summary>
    /// Logique d'interaction pour ViewAdd.xaml
    /// </summary>
    public partial class ViewAdd : UserControl
    {
        public ViewAdd()
        {
            InitializeComponent();
        }
        private int getID()
        {
            BDDConnexion conn = new BDDConnexion();
            conn.connexionDirecte();
            conn.ConnectionConfig();

            try
            {
                using (SqlConnection connection = new SqlConnection(conn.connexion2))
                {
                    connection.Open();
                    string sql2 = $"SELECT TOP 1 * FROM {((ComboBoxItem)category_select.SelectedItem).Content.ToString()} ORDER BY id DESC";
                    using (SqlCommand command = new SqlCommand(sql2, connection))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                int param1 = Int32.Parse(reader["id"].ToString()); //type int de base
                                return param1;
                            }
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                MessageBox.Show(e.ToString());
            }
            return -1;
        }

        private void Insert1()
        {
            BDDConnexion conn = new BDDConnexion();
            conn.connexionDirecte();
            conn.ConnectionConfig();

            try
            {
                using (SqlConnection connection = new SqlConnection(conn.connexion2))
                {
                    using (SqlCommand command = new SqlCommand())
                    {
                        command.Connection = connection;            // <== lacking
                        command.CommandType = CommandType.Text;
                        command.CommandText = "INSERT into Protein (id, nom, fonction, statut) VALUES (@idd, @name, @fonc, @rev)";
                        command.Parameters.AddWithValue("@idd", getID() + 1);
                        command.Parameters.AddWithValue("@name", txtPseudo.Text);
                        command.Parameters.AddWithValue("@fonc", txtFunc.Text);
                        command.Parameters.AddWithValue("@rev", 1);
                        try
                        {
                            connection.Open();
                            int recordsAffected = command.ExecuteNonQuery();
                        }
                        catch (SqlException exep)
                        {
                            MessageBox.Show(exep.ToString());
                        }
                        finally
                        {
                            connection.Close();
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void Insert2()
        {
            BDDConnexion conn = new BDDConnexion();
            conn.connexionDirecte();
            conn.ConnectionConfig();

            try
            {
                using (SqlConnection connection = new SqlConnection(conn.connexion2))
                {
                    using (SqlCommand command = new SqlCommand())
                    {
                        command.Connection = connection;            // <== lacking
                        command.CommandType = CommandType.Text;
                        command.CommandText = "INSERT into Gene (id, nom, sequence, organisme, statut) VALUES (@idd, @name, @seq, @org, @rev)";
                        command.Parameters.AddWithValue("@idd", getID() + 1);
                        command.Parameters.AddWithValue("@name", txtPseudo.Text);
                        command.Parameters.AddWithValue("@seq", blcseq.Text);
                        command.Parameters.AddWithValue("@org", blcorg.Text);
                        command.Parameters.AddWithValue("@rev", 1);
                        try
                        {
                            connection.Open();
                            int recordsAffected = command.ExecuteNonQuery();
                        }
                        catch (SqlException exep)
                        {
                            MessageBox.Show(exep.ToString());
                        }
                        finally
                        {
                            connection.Close();
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private int Insert3()
        {
            return 1;
        }

        private int Insert4()
        {
            return 1;
        }

        private int Insert5()
        {
            return 1;
        }

        private int Insert6()
        {
            return 1;
        }

        private void clickInsert(object sender, RoutedEventArgs e)
        {

            switch (((ComboBoxItem)category_select.SelectedItem).Content.ToString())
            {
                case "Protein":
                    Insert1();
                    break;

                case "Gene":
                    Insert2();
                    break;

                case "Sequence":
                    Insert3();
                    break;

                case "Structure":
                    Insert4();
                    break;

                case "Reaction":
                    Insert5();
                    break;

                case "Voie":
                    Insert6();
                    break;

                default:
                    break;
            }
        }
    }
}
