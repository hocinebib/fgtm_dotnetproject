﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SimpleNavigation.View
{
    /// <summary>
    /// Interaction logic for ViewB.xaml
    /// </summary>
    public partial class ViewB : UserControl
    {
        public ViewB()
        {
            InitializeComponent();

            BDDConnexion conn = new BDDConnexion();
            conn.connexionDirecte();
            conn.ConnectionConfig();

            try
            {
                using (SqlConnection connection = new SqlConnection(conn.connexion2))
                {
                    connection.Open();
                    string sql2 = "Select * from Protein";
                    using (SqlCommand command = new SqlCommand(sql2, connection))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                string param1 = reader["id"].ToString(); //type int de base
                                string param2 = reader.GetString(1); //type string de base
                                string param3 = reader.GetString(4); //type string de base
                                string param4 = reader["statut"].ToString();
                                param4 = (param4 == "0") ? "Not Reviewed" : "Reviewed";
                                Add_line(param1, param2, param3, param4);
                            }
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                MessageBox.Show(e.ToString());
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            var rowGroup = geneTable.RowGroups.FirstOrDefault();

            if (rowGroup != null)
            {
                TableRow row = new TableRow();

                TableCell cell = new TableCell();

                row.Background = System.Windows.Media.Brushes.AliceBlue;

                cell.Blocks.Add(new Paragraph(new Run("New prot ID")));
                row.Cells.Add(cell);

                cell = new TableCell();
                cell.Blocks.Add(new Paragraph(new Run("New prot Name")));
                row.Cells.Add(cell);

                cell = new TableCell();
                cell.Blocks.Add(new Paragraph(new Run("New prot function")));
                row.Cells.Add(cell);

                cell = new TableCell();
                cell.Blocks.Add(new Paragraph(new Run("Not Reviewed")));
                row.Cells.Add(cell);

                rowGroup.Rows.Add(row);
            }
        }

        private void Add_line(string id, string gene, string org, string rev)
        {
            var rowGroup = geneTable.RowGroups.FirstOrDefault();

            if (rowGroup != null)
            {
                TableRow row = new TableRow();

                TableCell cell = new TableCell();

                row.Background = System.Windows.Media.Brushes.AliceBlue;

                cell.Blocks.Add(new Paragraph(new Run(id)));
                row.Cells.Add(cell);

                cell = new TableCell();
                cell.Blocks.Add(new Paragraph(new Run(gene)));
                row.Cells.Add(cell);

                cell = new TableCell();
                cell.Blocks.Add(new Paragraph(new Run(org)));
                row.Cells.Add(cell);

                cell = new TableCell();
                cell.Blocks.Add(new Paragraph(new Run(rev)));
                row.Cells.Add(cell);

                rowGroup.Rows.Add(row);
            }
        }

    }
}
