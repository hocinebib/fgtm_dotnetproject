﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MindFusion.DataViews.Wpf;
using MindFusion.Diagramming.Wpf;
using MindFusion.Diagramming.Wpf.Layout;
using Orientation = MindFusion.Diagramming.Wpf.Layout.Orientation;

namespace SimpleNavigation.View
{
    /// <summary>
    /// Logique d'interaction pour ViewE.xaml
    /// </summary>
    public partial class ViewE : UserControl
    {
        public ViewE()
        {
            InitializeComponent();
            category_select.Items.Clear();
            SqlConnection con = new SqlConnection("Data Source=DESKTOP-DDTNQVJ\\SQLEXPRESS;Initial Catalog=fgtm;Integrated Security=True");
            try
            {
                con.Open();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
            try
            {
                SqlDataAdapter category_data = new SqlDataAdapter("SELECT * FROM Reaction", con);
                DataSet ds = new DataSet();
                category_data.Fill(ds, "t");

                category_select.DataContext = ds.Tables["t"].DefaultView;
                category_select.DisplayMemberPath = "id";
                category_select.SelectedValuePath = "id";

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void TurnReactionToNetwork(string idd)
        {
            List<Activity> activities = new List<Activity>();
            BDDConnexion conn = new BDDConnexion();
            conn.connexionDirecte();
            conn.ConnectionConfig();

            try
            {
                using (SqlConnection connection = new SqlConnection(conn.connexion2))
                {
                    connection.Open();
                    string sql2 = "Select * from Reaction where id = " + idd;
                    using (SqlCommand command = new SqlCommand(sql2, connection))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                string param2 = reader.GetString(1);
                                string param3 = reader.GetString(2);
                                string param4 = reader.GetString(3);
                                string param1 = reader["enzyme_id"].ToString();

                                activities.Add(new Activity { Name = param2, From = param3, To = param4 });
                                activities.Add(new Activity { Name = "Catalysation", From = param1, To = param3 });

                                BuildChart(activities);
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        void BuildChart(List<Activity> network)
        {
            diagram.BackBrush = new SolidColorBrush(Colors.White);

            // create diagram nodes for each milestone
            foreach (var activity in network)
            {
                if (diagram.FindNodeById(activity.From) == null)
                {
                    var node = diagram.Factory.CreateShapeNode(bounds, Shapes.Ellipse);
                    node.Id = node.Text = activity.From;
                }

                if (diagram.FindNodeById(activity.To) == null)
                {
                    var node = diagram.Factory.CreateShapeNode(bounds, Shapes.Ellipse);
                    node.Id = node.Text = activity.To;
                }
            }

            // create a diagram link for each activity
            foreach (var activity in network)
            {
                var link = diagram.Factory.CreateDiagramLink(
                    diagram.FindNodeById(activity.From),
                    diagram.FindNodeById(activity.To));

                var label1 = link.AddLabel(activity.Name);
                label1.RelativeTo = RelativeToLink.Segment;
                label1.Index = 0;
                label1.VerticalOffset = -20;
                label1.TextBrush = new SolidColorBrush(Colors.Red);
            }

            // arrange the diagram from left to right
            var layout = new LayeredLayout();
            layout.Margins = new Size(100, 100);
            layout.EnforceLinkFlow = true;
            layout.Orientation = Orientation.Horizontal;
            layout.Arrange(diagram);
        }

        Rect bounds = new Rect(0, 0, 130, 100); /* iciiiiii*/

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Button button = sender as Button;
            if (button.Name == "reac")
            {
                TurnReactionToNetwork(category_select.Text);
            }
        }
    }

    public class Activity
    {
        // activity name
        public string Name { get; set; }

        // start milestone name
        public string From { get; set; }

        // end milestone  name
        public string To { get; set; }

        // time in months
        public int Time { get; set; }
    }
}