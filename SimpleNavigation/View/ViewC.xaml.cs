﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Text.RegularExpressions;
using SimpleNavigation.Classes;

namespace SimpleNavigation.View
{
    /// <summary>
    /// Logique d'interaction pour ViewC.xaml
    /// </summary>
    public partial class ViewC : UserControl
    {
        public ViewC()
        {
            InitializeComponent();

            category_select.Items.Clear();
            SqlConnection con = new SqlConnection("Data Source=DESKTOP-DDTNQVJ\\SQLEXPRESS;Initial Catalog=fgtm;Integrated Security=True");
            try
            {
                con.Open();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
            try
            {
                SqlDataAdapter category_data = new SqlDataAdapter("SELECT * FROM Sequence", con);
                DataSet ds = new DataSet();
                category_data.Fill(ds, "t");

                category_select.DataContext = ds.Tables["t"].DefaultView;
                category_select.DisplayMemberPath = "id";
                category_select.SelectedValuePath = "id";

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }

        }

        private void ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            BDDConnexion conn = new BDDConnexion();
            conn.connexionDirecte();
            conn.ConnectionConfig();

            try
            {
                using (SqlConnection connection = new SqlConnection(conn.connexion2))
                {
                    connection.Open();
                    string sql2 = "Select * from Sequence where id = " + category_select.Text;
                    using (SqlCommand command = new SqlCommand(sql2, connection))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                string param3 = reader.GetString(2);
                                string param1 = reader["id_prot"].ToString();
                                param3 = GetProtInfo(param1) + SpliceText(param3, 70);
                                tbSettingText.Text = param3;
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private string GetProtInfo(string protid)
        {
            BDDConnexion conn = new BDDConnexion();
            conn.connexionDirecte();
            conn.ConnectionConfig();

            try
            {
                using (SqlConnection connection = new SqlConnection(conn.connexion2))
                {
                    connection.Open();
                    string sql2 = "Select * from Protein where id = " + protid;
                    using (SqlCommand command = new SqlCommand(sql2, connection))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                string param2 = reader.GetString(1); //type string de base
                                string param3 = reader.GetString(4); //type string de base
                                return $">{param2} | {param3}\n";
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.ToString());
            }
            return "> | \n";
        }

        public static string SpliceText(string text, int lineLength)
        {
            return Regex.Replace(text, "(.{" + lineLength + "})", "$1" + Environment.NewLine);
        }

    }
}
