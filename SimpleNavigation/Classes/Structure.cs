﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SimpleNavigation.Classes
{
    internal class Structure
    {
        private int id;
        private Protein prot_id;
        private string name;
        private string structure;
        private bool status = false;

        public Structure()
        {

        }

        // Constructeur vide surchargé par 5 paramétres.
        public Structure(int ident, Protein protid, string nom, string strct)
        {
            this.id = ident;
            this.prot_id = protid;
            this.name = nom;
            this.structure = strct;
        }

        public void Setid(int ident)
        {
            this.id = ident;
        }
        public void Setprot(Protein protid)
        {
            this.prot_id = protid;
        }
        public void Setname(string nom)
        {
            this.name = nom;
        }
        public void Setsequence(string strct)
        {
            this.structure = strct;
        }
        public void Setstatus(bool stat)
        {
            this.status = stat;
        }

        public int Getid()
        {
            return this.id;
        }

        public Protein GetProtein()
        {
            return this.prot_id;
        }
        public string Getname()
        {
            return this.name;
        }
        public string Getstructure()
        {
            return this.structure;
        }

        public bool Getstatus()
        {
            return this.status;
        }

        public override string ToString()
        {
            return $"{this.name}\\n{this.prot_id.Getname()}\\n{this.id}";
        }
    }
}
