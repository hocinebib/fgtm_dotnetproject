﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SimpleNavigation.Classes
{
    internal class Sequence
    {
        private int id;
        private Protein prot_id;
        private string name;
        private string sequence;
        private bool status  = false;

        public Sequence()
        {

        }

        // Constructeur vide surchargé par 5 paramétres.
        public Sequence(int ident, Protein protid, string nom, string seq)
        {
            this.id = ident;
            this.prot_id = protid;
            this.name = nom;
            this.sequence = seq;
        }

        public void Setid(int ident)
        {
            this.id = ident;
        }
        public void Setprot(Protein protid)
        {
            this.prot_id = protid;
        }
        public void Setname(string nom)
        {
            this.name = nom;
        }
        public void Setsequence(string seq)
        {
            this.sequence = seq;
        }
        public void Setstatus(bool stat)
        {
            this.status = stat;
        }

        public int Getid()
        {
            return this.id;
        }

        public Protein GetProtein()
        {
            return this.prot_id;
        }
        public string Getname()
        {
            return this.name;
        }
        public string Getsequence()
        {
            return this.sequence;
        }

        public bool Getstatus()
        {
            return this.status;
        }

        public override string ToString()
        {
            return $"> {this.name} | {this.prot_id.Getname()} \\n {this.sequence}";
        }
    }
}
