﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SimpleNavigation.Classes
{
    internal class Protein
    {
        private int id;
        private string name;
        private Sequence id_seq;
        private Structure id_struct;
        private string fonction;
        private bool status = false;

        public Protein()
        {

        }

        // Constructeur vide surchargé par 5 paramétres.
        public Protein(int ident, string nom, Sequence seq, Structure strct, string funct)
        {
            this.id = ident;
            this.name = nom;
            this.id_seq = seq;
            this.id_struct = strct;
            this.fonction = funct;
        }

        public void Setid(int ident)
        {
            this.id = ident;
        }
        public void Setname(string nom)
        {
            this.name = nom;
        }
        public void Setsequence(Sequence seq)
        {
            this.id_seq = seq;
        }
        public void SetStructure(Structure strct)
        {
            this.id_struct = strct;
        }
        public void Setfonction(string fct)
        {
            this.fonction = fct;
        }
        public void Setstatus(bool stat)
        {
            this.status = stat;
        }

        public int Getid()
        {
            return this.id;
        }
        public string Getname()
        {
            return this.name;
        }
        public Sequence Getsequence()
        {
            return this.id_seq;
        }
        public Structure Getstructure()
        {
            return this.id_struct;
        }
        public string Getfct()
        {
            return this.fonction;
        }
        public bool Getstatus()
        {
            return this.status;
        }

        public override string ToString()
        {
            return $"Protein Name : {this.name} \\nProtein Sequence : {this.id_seq.Getid()}\\nProtein Structure : {this.id_struct.Getid()} \\n" +
                $"Protein Function : {this.fonction} \\nProtein ID : {this.id}";
        }
    }
}
