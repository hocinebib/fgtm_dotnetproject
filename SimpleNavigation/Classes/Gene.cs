﻿using System;
using System.Collections.Generic;
using System.Numerics;
using System.Text;

namespace SimpleNavigation.Classes
{
    internal class Gene
    {
        private int id;
        private string name;
        private string sequence;
        private int position;
        private string organism;
        private bool status = false;

        public Gene()
        {

        }

        // Constructeur vide surchargé par 5 paramétres.
        public Gene(int ident, string nom, string seq, int pos, string org)
        {
            this.id = ident;
            this.name = nom;
            this.sequence = seq;
            this.position = pos;
            this.organism = org;
        }

        public void Setid(int ident)
        {
            this.id = ident;
        }
        public void Setname(string nom)
        {
            this.name = nom;
        }
        public void Setsequence(string seq)
        {
            this.sequence = seq;
        }
        public void Setpos(int pos)
        {
            this.position = pos;
        }
        public void Setorg(string org)
        {
            this.organism = org;
        }
        public void Setstatus(bool stat)
        {
            this.status = stat;
        }

        public int Getid()
        {
            return this.id;
        }
        public string Getname()
        {
            return this.name;
        }
        public string Getsequence()
        {
            return this.sequence;
        }
        public int Getpos()
        {
            return this.position;
        }
        public string Getorg()
        {
            return this.organism;
        }
        public bool Getstatus()
        {
            return this.status;
        }

        public override string ToString()
        {
            return $"> {this.name} | {this.organism} | {this.position} \\n {this.sequence}";
        }

    }
}
