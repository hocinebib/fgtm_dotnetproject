﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SimpleNavigation.Classes
{
    internal class Reaction
    {
        private int id;
        private string name;
        private List<Protein> enzymes;
        private List<string> reactifs;
        private List<string> resultats;
        private string organism;
        private bool status = false;

        public Reaction()
        {

        }

        // Constructeur vide surchargé par 5 paramétres.
        public Reaction(int ident, string nom, List<Protein> enzy, List<string> rea, List<string> res, string org)
        {
            this.id = ident;
            this.name = nom;
            this.enzymes = enzy;
            this.reactifs = rea;
            this.resultats = res;
            this.organism = org;
        }

        public void Setid(int ident)
        {
            this.id = ident;
        }
        public void Setname(string nom)
        {
            this.name = nom;
        }
        public void Setenzymes(List<Protein> enzy)
        {
            foreach(Protein p in enzy)
            {
                this.enzymes.Add(p);
            }
        }
        public void Setreactifs(List<string> rea)
        {
            foreach (string r in rea)
            {
                this.reactifs.Add(r);
            }
        }
        public void Setrresultats(List<string> res)
        {
            foreach (string rs in res)
            {
                this.resultats.Add(rs);
            }
        }
        public void Setorg(string org)
        {
            this.organism = org;
        }
        public void Setstatus(bool stat)
        {
            this.status = stat;
        }

        public int Getid()
        {
            return this.id;
        }
        public string Getname()
        {
            return this.name;
        }
        public List<Protein> Getenzymes()
        {
            return this.enzymes;
        }
        public List<string> Getreactifs()
        {
            return this.reactifs;
        }
        public List<string> Getresultats()
        {
            return this.resultats;
        }
        public string Getorg()
        {
            return this.organism;
        }
        public bool Getstatus()
        {
            return this.status;
        }

        public override string ToString()
        {
            string lestring = "";
            lestring = lestring + $"La Reaction : {this.name}\\nTransforme :";
            foreach(string r in this.reactifs)
            {
                lestring = lestring + r + "\\n";
            }
            lestring = lestring + "Catalysé par :";

            foreach (Protein p in this.enzymes)
            {
                lestring = lestring + p.Getname() + "\\n";
            }

            lestring = lestring + "En :";

            foreach(string r in this.resultats)
            {
                lestring = lestring + r + "\\n";
            }

            return lestring;
        }
    }
}
