﻿using FGTM.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Controls;
using System.Windows.Media;

namespace FGTM.Service
{
    public class NavigationService
    {

        public List<NavigationModel> NavigationOptions { get => NavigationNameToUserControl.Keys.ToList(); }

        public UserControl NavigateToModel(NavigationModel _navigationModel)
        {
            if (_navigationModel is null)
                //Or throw exception
                return null;
            if (NavigationNameToUserControl.ContainsKey(_navigationModel))
            {
                return NavigationNameToUserControl[_navigationModel].Invoke();
            }
            //Ideally you should throw here Custom Exception
            return null;
        }

        //Usage of the Func, provides each call new initialization of the view
        //If you need initialized views, just remove the Func
        //-------------------------------------------------------------------
        //Readonly is used only for performance reasons
        //Of course there is option to add the elements to the collection, if dynamic navigation mutation is needed
        private readonly Dictionary<NavigationModel, Func<UserControl>> NavigationNameToUserControl = new Dictionary<NavigationModel, Func<UserControl>>
        {
            { new NavigationModel("Genes","This will navigate to the Gene View",Brushes.GreenYellow), ()=>{ return new View.GeneView(); } },
            { new NavigationModel("Proteins","This will navigate to the Prot View",Brushes.GreenYellow), ()=>{ return new View.ProtView(); } }
        };

        #region SingletonThreadSafe
        private static readonly object Instancelock = new object();

        private static NavigationService instance = null;

        public static NavigationService GetInstance
        {
            get
            {
                if (instance == null)
                {
                    lock (Instancelock)
                    {
                        if (instance == null)
                        {
                            instance = new NavigationService();
                        }
                    }
                }
                return instance;
            }
        }
        #endregion
    }
}
