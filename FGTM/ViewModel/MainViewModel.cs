﻿using FGTM.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Navigation;

namespace FGTM.ViewModel
{
    public class MainViewModel
    {
        public List<NavigationModel> NavigationOptions { get => NavigationService.GetInstance.NavigationOptions; }

    }
}
