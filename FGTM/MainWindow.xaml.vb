﻿'
' Copyright (c) 2021, MindFusion LLC - Bulgaria.
'

Imports MindFusion.Diagramming.Wpf.Layout


''' <summary>
''' Interaction logic for MainWindow.xaml
''' </summary>
Partial Public Class MainWindow
	Inherits Window
	Public Sub New()
		InitializeComponent()
	End Sub

	Private Sub OnWindowLoaded(ByVal sender As Object, ByVal e As RoutedEventArgs)
		Dim nodeMap = New Dictionary(Of String, DiagramNode)()
		Dim bounds = New Rect(0, 0, 60, 22)

		' load the graph xml
		Dim xml = XDocument.Load("SampleGraph.xml")
		Dim graph = xml.Element("Graph")

		' load node data
		Dim nodes = graph.Descendants("Node")
		For Each node As XElement In nodes
			Dim diagramNode = diagram.Factory.CreateShapeNode(bounds)
			nodeMap(node.Attribute("id").Value) = diagramNode
			diagramNode.Text = node.Attribute("name").Value
		Next

		' load link data
		Dim links = graph.Descendants("Link")
		For Each link As XElement In links
			diagram.Factory.CreateDiagramLink(nodeMap(link.Attribute("origin").Value), nodeMap(link.Attribute("target").Value))
		Next

		' arrange the graph
		Dim layout = New LayeredLayout()
		layout.Arrange(diagram)
	End Sub
End Class