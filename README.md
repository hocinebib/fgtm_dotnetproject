# FGTM (From Gene To Metabolism)
C# WPF application playing the role of metabolic pathways database allowing the access to the reactions participating in the pathway but also the genes, the protein sequences and protein structures of the enzymes of each reaction.

---

## Description :
metabolic pathways database allowing the access to the reactions participating in the pathway but also the genes, the protein sequences and protein structures of the enzymes of each reaction.

## Requirements :

### System :
* Windows
* Visual Studio

## Usage :
1. First clone this repository :
```shell
$git clone https://gitlab.com/hocinebib/fgtm_dotnetproject.git
```
or [download it](https://gitlab.com/hocinebib/fgtm_dotnetproject/-/archive/main/fgtm_dotnetproject-main.zip).



